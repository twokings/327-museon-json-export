<?php

namespace Bolt\Extension\TwoKings\MuseonJsonExport;

use Bolt\Extension\SimpleExtension;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * MuseonJsonExport extension class.
 *
 * @author Lodewijk Evers <lodewijk@twokings.nl>
 */
class MuseonJsonExportExtension extends SimpleExtension
{

    /**
     * Register the routes set in the configuration
     *
     * @param ControllerCollection $collection
     */
    protected function registerFrontendRoutes(ControllerCollection $collection)
    {
        $config = $this->getConfig();
        foreach($config['endpoints'] as $name => $endpoint) {
            $endpoint_path = '/'. $endpoint['path'];
            // All requests to /endpoint
            $collection->match($endpoint_path, [$this, 'callbackJsonExport'])
                ->value('endpoint', $endpoint)
                ->bind('museon_export_route_'. $name);
            $collection->match('/en' . $endpoint_path, [$this, 'callbackJsonExport'])
                ->value('endpoint', $endpoint)
                ->value('lang', 'en')
                ->bind('museon_export_route_en_'. $name);
            $collection->match('/nl' . $endpoint_path, [$this, 'callbackJsonExport'])
                ->value('endpoint', $endpoint)
                ->value('lang', 'nl')
                ->bind('museon_export_route_nl_'. $name);
            $collection->match($endpoint_path .'/{id}', [$this, 'callbackJsonExport'])
                ->value('endpoint', $endpoint)
                ->bind('museon_export_route_detail_'. $name);
            $collection->match('/en' . $endpoint_path .'/{id}', [$this, 'callbackJsonExport'])
                ->value('endpoint', $endpoint)
                ->value('lang', 'en')
                ->bind('museon_export_route_detail_en_'. $name);
            $collection->match('/nl' . $endpoint_path .'/{id}', [$this, 'callbackJsonExport'])
                ->value('endpoint', $endpoint)
                ->value('lang', 'nl')
                ->bind('museon_export_route_detail_nl_'. $name);
        }

    }

    /**
     * Handle some output
     *
     * @param Application $app
     * @param Request $request
     * @return Response
     */
    public function callbackJsonExport(Application $app, Request $request, $endpoint = [], $lang = 'nl')
    {
        $hostkeys['name'] = $request->server->get('HTTP_HOST');
        $hostkeys['scheme'] = $request->server->get('REQUEST_SCHEME');
        $hostkeys['filespath'] = '/files/';
        $hostpath = $hostkeys['scheme'] . '://' . $hostkeys['name'] . $hostkeys['filespath'];

        $contenttype = $endpoint['contenttype'];

        $app_tag = $request->query->get('field_app_tag')? $request->query->get('field_app_tag') : null;

        $id = $request->attributes->get('id')? $request->attributes->get('id') : null;

        $repo = $app['storage']->getRepository($contenttype);

        if ($id == null) {
            $_items = [];

            $qb = $repo->createQueryBuilder();

            // basically fetch only published items
            $qb->where('status = :status')->setParameter('status', 'published');

            // if it's english = fetch only english items
            // or else only fetch dutch items
            if ($lang == 'en') {
                //$qb->andWhere('nlslug != enslug');
                $qb->andWhere('api_id_en > 0');
            } else {
                $qb->andWhere('api_id_nl > 0');
            }

            if ($app_tag != null) {
                $qb->andWhere('app_tag = :apptagvalue')->setParameter('apptagvalue', $app_tag, \PDO::PARAM_INT);
            }

            $qb->orderBy('title', 'DESC');
            //dump($qb);
            //dump($qb->__toString());

            $records = $qb->execute()->fetchAll();
            //dump($records);

            foreach($records as $key=> $item) {
                $this->translateItem($item, $lang);
                $this->cleanupItem($item);

                $out = [
                    'id' => $item['entity_id'],
                    'title' => $item['title'],
                    'subtitle' => ($item['subtitle'])? $item['subtitle'] : '',
                    'image' => (is_array($item['image']) && array_key_exists('file', $item['image']) && $item['image']['file'])? $hostpath . $item['image']['file'] : '' ,
                    'app_tag' =>  ($item['app_tag'])?$item['app_tag']:'[]',
                    'language' => $item['locale'],
                    '_bolt_item' => $item,
                ];
                $_items[$key] = $out;
            }
            $items = $_items;
        } else {
            if ($lang == 'en') {
                $item = $repo->findOneBy(['api_id_en' => $id]);
            } else {
                $item = $repo->findOneBy(['api_id_nl' => $id]);
            }

            $this->translateItem($item, $lang);
            $this->cleanupItem($item);

            $_item = [
                'title' => $item['title'],
                'field_article_images' => [],
                'field_tag' => [],
                'text' => $item['json_body'],
                'language' => $item['locale'],
                'app_tag' => ($item['app_tag'])?$item['app_tag']:'[]',
                'image_url' => (array_key_exists('file', $item['image']) && $item['image']['file'])? $hostpath . $item['image']['file'] : '' ,
                'image_urls' => [
                    [
                        'image_url' => ($item['image']['file'])? $hostpath . $item['image']['file'] : '' ,
                        'title' => array_key_exists('title', $item['image'])? $item['image']['title'] : '' ,
                    ],
                ],
                'subtitle' => ($item['subtitle'])? $item['subtitle'] : '',
                'id' => $item['entity_id'],
                '_bolt_item' => $item,
            ];
            $items = $_item;
        }


        return new JsonResponse($items);
    }

    /**
     * @param $item
     */
    function cleanupItem(&$item) {
        // strip tags and spaces that are unused
        $item['json_body'] = strip_tags(
            str_replace("</p>", "</p>\n",
                str_replace('&nbsp;', ' ' ,$item['body'])
            )
        );

        // some items might not have an image
        // make sure they have an empty image field
        if (!array_key_exists('image', $item)) {
            $item['image'] = array('file' => false);
        }

        $item['endata'] = (array)json_decode($item['endata']);
        $item['nldata'] = (array)json_decode($item['nldata']);
    }

    /**
     * Replace item with english translation if lang = en
     *
     * @param $item
     * @param string $lang
     */
    function translateItem(&$item, $lang = 'nl') {
        //dump($item);
        if($lang == 'en' && $enitem = (array)json_decode($item['endata'])) {
            $item['entity_id'] = $item['api_id_en'];
            $item['title'] = (array_key_exists('title', $enitem) && $enitem['title'])? $enitem['title']: '';
            $item['subtitle'] = (array_key_exists('subtitle', $enitem) && $enitem['subtitle'])? $enitem['subtitle']: '';
            $item['introduction'] = (array_key_exists('introduction', $enitem) && $enitem['introduction'])? $enitem['introduction']: '';
            $item['body'] = (array_key_exists('body', $enitem) && $enitem['body'])? $enitem['body']: '';
            $item['locale'] = 'en';
        } else {
            $nlitem = (array)json_decode($item['nldata']);
            $item['entity_id'] = $item['api_id_nl'];
            $item['title'] = (array_key_exists('title', $nlitem) && $nlitem['title'])? $nlitem['title']: $item['title'];
            $item['subtitle'] = (array_key_exists('subtitle', $nlitem) && $nlitem['subtitle'])? $nlitem['subtitle']: '';
            $item['introduction'] = (array_key_exists('introduction', $nlitem) && $nlitem['introduction'])? $nlitem['introduction']: $item['introduction'];
            $item['body'] = (array_key_exists('body', $nlitem) && $nlitem['body'])? $nlitem['body']: $item['body'];
            $item['locale'] = 'nl';
        }
    }

    /**
     * Read the configuration
     *
     * @return array
     */
    public function getConfig()
    {
        return parent::getConfig();
    }

    /**
     * Provide default configuration
     *
     * @return array
     */
    protected function getDefaultConfig()
    {
        return [
            'endpoints' => [
                'knowledgebase' => [
                    'contenttype' => 'knowledgebase',
                    'path' => 'museon_json/knowledgebase_article'
                ],
            ]
        ];
    }
}
